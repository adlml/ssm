import autograd.numpy as np
import autograd.numpy.random as npr
npr.seed(0)

import matplotlib
import matplotlib.pyplot as plt

import ssm
from ssm.util import find_permutation

# Set the parameters of the HMM
T = 500     # number of time bins
K = 5       # number of discrete states
D = 2       # number of observed dimensions

# Make an HMM with the true parameters
true_hmm = ssm.HMM(K, D, observations="bernoulli")
z, y = true_hmm.sample(T)
z_test, y_test = true_hmm.sample(T)
true_ll = true_hmm.log_probability(y)

# Fit models
N_sgd_iters = 1000
N_em_iters = 100

# A bunch of observation models that all include the
# diagonal Gaussian as a special case.
results = {}
method = "em"
obs = "bernoulli"

print("Fitting {} HMM with {}".format(obs, method))
model = ssm.HMM(K, D, observations=obs)
train_lls = model.fit(y, method=method)
test_ll = model.log_likelihood(y_test)
smoothed_y = model.smooth(y)



# Permute to match the true states
model.permute(find_permutation(z, model.most_likely_states(y)))


# sample some states
_, samples = true_hmm.sample(5)

#pxnp1 = model.predict_xnp1(y, x=samples)
#expected_states = model.predict_xnp1_2(y, x=samples)
pxnp1_3 = model.predict_xnp1_3(y, x=samples)
#print('xnp1: ' , pxnp1)
print('xnp1_3: ' , pxnp1_3)
